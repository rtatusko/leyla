package com.leyla.dto;

import lombok.Data;

@Data
public class UseParkingDTO {
    private String parkingId;
    private String userName;
}
