package com.leyla.dto;

import lombok.Data;

@Data
public class ParkingViewDTO {

    private String id;
    private String name;
    private String street;
    private boolean free;

}
