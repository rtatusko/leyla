package com.leyla.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
@MappedSuperclass
public abstract class IdEntity implements Serializable {

	@Id
	@GeneratedValue(generator = "system-uuid", strategy = GenerationType.IDENTITY)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id", nullable = false, updatable = false)
	protected String id;

	public IdEntity(String id) {
		this.id = id;
	}
	
}
