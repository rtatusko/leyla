package com.leyla.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "parking_history")
public class ParkingHistoryEntity extends IdEntity{

    @ManyToOne
    @JoinColumn(name = "parking_id")
    private ParkingEntity parking;

    private String userName;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "using_from", updatable = false)
    @CreationTimestamp
    private Date from;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "using_to")
    private Date to;

    public ParkingHistoryEntity(ParkingEntity saved, String userName) {
        this.parking = saved;
        this.userName = userName;
    }
}
