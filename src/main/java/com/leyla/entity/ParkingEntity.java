package com.leyla.entity;

import com.leyla.dto.ParkingDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "parking")
public class ParkingEntity extends IdEntity {

    private String name;

    private String street;

    private boolean free = true;





    public ParkingEntity(ParkingDTO dto) {
        this.name = dto.getName();
    }
}
