package com.leyla.exeption;

import org.springframework.http.HttpStatus;

public class ConflictException extends RESTException {

    public ConflictException() {
        this(HttpStatus.CONFLICT.getReasonPhrase());
    }

    public ConflictException(String message) {
        super(HttpStatus.CONFLICT.value(), message);
    }
}
