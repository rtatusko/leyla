package com.leyla.exeption;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class RESTException extends RuntimeException {

	private int code;
	private String message;

	public RESTException() {
		super();
	}

	public RESTException(int code, Exception ex) {
		this(code, ex.getLocalizedMessage());
		super.setStackTrace(ex.getStackTrace());
	}

	public RESTException(int code, String message) {
		this.code = code;
		this.message = message;
	}
}
