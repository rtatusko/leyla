package com.leyla.exeption;

import org.springframework.http.HttpStatus;


public class NotFoundException extends RESTException {

    public NotFoundException() {
        this(HttpStatus.NOT_FOUND.getReasonPhrase());
    }

    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND.value(), message);
    }
}
