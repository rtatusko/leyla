package com.leyla.exeption.handling;


import com.leyla.exeption.RESTException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletResponse;

import static com.leyla.exeption.handling.ExceptionConstants.API_ERROR;

@Slf4j
@ControllerAdvice
public class ExceptionControllerAdvice extends LeylaControllerAdvice {

	@ResponseBody
	@ExceptionHandler(Throwable.class)
	public RestExceptionDTO genericExceptionHandler(HttpServletResponse resp, Throwable ex) {
		log.error("API exception", ex);
		return buildExceptionDTO(resp, API_ERROR, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@ExceptionHandler(IllegalArgumentException.class)
	public RestExceptionDTO illegalArgumentExceptionHandler(HttpServletResponse resp, IllegalArgumentException ex) {
		return buildExceptionDTO(resp, API_ERROR, ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@ExceptionHandler(InvalidDataAccessApiUsageException.class)
	public RestExceptionDTO invalidDataAccessApiUsageExceptionHandler(HttpServletResponse resp, InvalidDataAccessApiUsageException ex) {
		return buildExceptionDTO(resp, API_ERROR, ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public RestExceptionDTO methodArgumentTypeMismatchExceptionHandler(HttpServletResponse resp, MethodArgumentTypeMismatchException ex) {
		return buildExceptionDTO(resp, API_ERROR, ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@ExceptionHandler(RESTException.class)
	public RestExceptionDTO restExceptionHandler(HttpServletResponse resp, RESTException ex) {
		String errorMessage = messageResolver.getMessage(ex.getMessage());
		log.error(errorMessage);
		return restExceptionHandler(resp, ex.getCode(), errorMessage);
	}

	private RestExceptionDTO restExceptionHandler(HttpServletResponse resp, int code, String errorMessage) {
		resp.setStatus(code);
		RestExceptionDTO result = new RestExceptionDTO();
		result.setMessage(errorMessage);
		result.setType(HttpStatus.valueOf(code).name());
		result.setCode(code);
		return result;
	}

}
