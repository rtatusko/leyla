package com.leyla.exeption.handling;

import com.leyla.helper.MessageResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;

public class LeylaControllerAdvice {

	@Autowired
	protected MessageResolver messageResolver;

	protected RestExceptionDTO buildExceptionDTO(HttpServletResponse response, String code, String description, HttpStatus status) {
		String errorMessage = messageResolver.getMessage(code);
		RestExceptionDTO result = new RestExceptionDTO();
		result.setMessage(errorMessage);
		response.setStatus(status.value());
		result.setType(status.getReasonPhrase());
		result.setCode(status.value());
		result.setDetails(description);
		return result;
	}

}
