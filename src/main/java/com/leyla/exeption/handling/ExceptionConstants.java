package com.leyla.exeption.handling;

public class ExceptionConstants {

	private ExceptionConstants() {
		throw new IllegalStateException();
	}

	public static final String API_ERROR = "api.error";
}
