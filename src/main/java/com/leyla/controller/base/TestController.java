package com.leyla.controller.base;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class TestController {

    @GetMapping("/test")
    public ResponseEntity resendInvitation() {
        return ResponseEntity.ok().body("some");
    }

}
