package com.leyla.controller;

import com.leyla.dto.ParkingDTO;
import com.leyla.dto.ParkingViewDTO;
import com.leyla.dto.UseParkingDTO;
import com.leyla.entity.ParkingEntity;
import com.leyla.repository.ParkingRepository;
import com.leyla.service.ParkingService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/parking")
public class ParkingController {

    private final ParkingService parkingService;
    private final ParkingRepository parkingRepository;
    private ModelMapper modelMapper;

    //configuration data

    @PostMapping()
    public ResponseEntity saveParking(@RequestBody ParkingDTO dto) {
        return ResponseEntity.ok().body(parkingRepository.save(new ParkingEntity(dto)));
    }

    //    ---

    @GetMapping()
    public List<ParkingViewDTO> getAll() {
        return parkingRepository.findAll()
                .stream()
                .map(a -> modelMapper.map(a, ParkingViewDTO.class))
                .collect(Collectors.toList());
    }


    @PutMapping("/use")
    public void use(@RequestBody UseParkingDTO dto) {
        parkingService.useParking(dto);
    }

    @PutMapping("/finish-using")
    public void finishUsing(@RequestBody UseParkingDTO dto) {
        parkingService.finishUsing(dto);
    }


}
