package com.leyla.service;

import com.leyla.dto.UseParkingDTO;
import com.leyla.entity.ParkingEntity;
import com.leyla.entity.ParkingHistoryEntity;
import com.leyla.exeption.ConflictException;
import com.leyla.exeption.NotFoundException;
import com.leyla.repository.ParkingHistoryRepository;
import com.leyla.repository.ParkingRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Slf4j
@Service
@AllArgsConstructor
public class ParkingService {

    private final ParkingRepository parkingRepository;
    private final ParkingHistoryRepository parkingHistoryRepository;

    @Transactional
    public void useParking(UseParkingDTO dto) {
        ParkingEntity parkingEntity = parkingRepository.findById(dto.getParkingId()).orElseThrow(NotFoundException::new);

        if (!parkingEntity.isFree()) {
            throw new ConflictException("Parking is not free");
        }

        parkingEntity.setFree(false);
        ParkingEntity saved = parkingRepository.save(parkingEntity);

        ParkingHistoryEntity parkingHistoryEntity = new ParkingHistoryEntity(saved, dto.getUserName());
        parkingHistoryRepository.save(parkingHistoryEntity);
        log.info("start use -> user {} ", dto.getUserName());
    }

    @Transactional
    public void finishUsing(UseParkingDTO dto) {
        ParkingEntity parkingEntity = parkingRepository.findById(dto.getParkingId()).orElseThrow(NotFoundException::new);
        ParkingHistoryEntity parkingHistoryEntity = this.parkingHistoryRepository.findByParking_IdAndToIsNull(parkingEntity.getId()).orElseThrow(ConflictException::new);

        parkingEntity.setFree(true);
        if (!parkingHistoryEntity.getUserName().equalsIgnoreCase(dto.getUserName())) {
            throw new ConflictException("Conflict on parking history");
        }

        parkingHistoryEntity.setTo(new Date());
        parkingHistoryRepository.save(parkingHistoryEntity);

        parkingRepository.save(parkingEntity);
        log.info("Finish use -> user {} ", dto.getUserName());
    }
}
