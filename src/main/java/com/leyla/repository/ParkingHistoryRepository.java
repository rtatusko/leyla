package com.leyla.repository;

import com.leyla.entity.ParkingHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ParkingHistoryRepository extends JpaRepository<ParkingHistoryEntity, String> {

    Optional<ParkingHistoryEntity> findByParking_IdAndToIsNull(String parkingId);

}
