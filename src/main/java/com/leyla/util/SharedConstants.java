package com.leyla.util;

public interface SharedConstants {

	String BEARER = "Bearer ";
	String UNKNOWN = "Unknown";

	String DATE_FORMAT_PATTERN_WIDE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String DATE_FORMAT_PATTERN_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    String DATE_FORMAT_PATTERN_SLASH_DDMMYYYY = "dd/MM/yyyy";
    String DATE_FORMAT_PATTERN_DASH_YYYY_MM_DD = "yyyy-MM-dd";

}
